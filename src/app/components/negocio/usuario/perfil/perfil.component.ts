import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsuarioService } from '../../../../services/negocio/usuario/registro/usuario.service';
import { usuario } from '../../../../models/negocio/usuario/usuario.model';
import { ComercioService } from 'src/app/services/negocio/usuario/comercio/comercio.service';
import { Comercio } from 'src/app/models/negocio/comercio/comercio.model';
import { FormBuilder, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { DiasAtencionService } from 'src/app/services/negocio/comercio/registro/dias-atencion.service';
import { DiasAtencion } from 'src/app/models/negocio/comercio/dias-atencion.model';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {

  usuario: usuario = new usuario();
  comercios: Comercio[];
  diasAtencion: DiasAtencion = new DiasAtencion();
  diasArr: DiasAtencion[];
  constructor(private router: Router, private service: UsuarioService, private comercio: ComercioService, private formBuilder: FormBuilder, private diasAtencionService: DiasAtencionService) { }

  ngOnInit(): void {
    this.editar();
  }

  editar(){
    this.service.getusuario(localStorage.getItem("userbyuser")).subscribe(data => {
      this.usuario = data;
      localStorage.setItem("id", this.usuario.id.toString());
      this.mostrarComercios();
    })
  }

  mostrarComercios(){
    let id = localStorage.getItem("id");
    this.comercio.getNegociobyIdUsuario(+id).subscribe(data => {
      this.comercios = data;
    })
  }

  actualizar(usuario: usuario){
    if(this.registerForm.get('nombre').status==='VALID' && this.registerForm.get('apellidoPaterno').status==='VALID' && this.registerForm.get('apellidoMaterno').status==='VALID'&& this.registerForm.get('correo').status==='VALID'&& this.registerForm.get('telefono').status==='VALID'){
      this.service.putusuario(usuario)
      .subscribe(data => {
        this.usuario = data;
        this.router.navigate(["perfil"]);
        Swal.fire('Datos personales actualizados correctamente.')
        this.editar();
      })
    }
    else{
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Alguna regla de validación no se está cumpliendo'
      })
    }
    
  }
  actualizarPassword(usuario: usuario){
    if(usuario.password===this.registerForm.get('passwordActual').value){
      if(this.registerForm.get('passwordNueva').status==='VALID' && this.registerForm.get('passwordConfirmacion').status==='VALID'){
        usuario.apellidoMaterno='';
        usuario.apellidoPaterno='';
        usuario.correo='';
        usuario.password=this.registerForm.get('passwordNueva').value;
        usuario.nombreUsuario='';
        usuario.telefono='';
        usuario.nombre='';
        this.service.putusuario(usuario)
        .subscribe(data => {
          this.router.navigate(["perfil"]);
          Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Se actualizo la contraseña correctamente',
            showConfirmButton: false,
            timer: 1500
          })
          window.location.reload();
        })

      }
      else{
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Las contraseñas tienen que coincidir!'
        })
      }
    }
    else{
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Contraseña actual incorrecta!'
      })
    }
  
  }

  datosUsuario:boolean=false;
  datosComercio:boolean=false;

  registroComercio(){
    this.router.navigate(["registro-negocio"]);
}

  datosUsuarioFunction(){
    this.datosUsuario = !this.datosUsuario;
}

  datosComercioFunction(){
    this.datosComercio = !this.datosComercio;
}

modificar(comercio: Comercio):void{
  localStorage.setItem("idComercio", comercio.id.toString());
  this.router.navigate(["actualizar-horario"]);
}

eliminar(comercio: Comercio){
   
Swal.fire({
  title: 'Eliminar Comercio',
  text: "¿Deseas eliminar este comercio?",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Si, eliminar!'
}).then((result) => {
  if (result.value) {
    this.diasAtencionService.getHorario2(comercio.id).subscribe(Resp => {
      this.diasArr = Resp;
      for (let index = 0; index < 7; index++) {
        this.diasAtencion.id = this.diasArr[index].id;
        console.log(this.diasAtencion.id);
        this.diasAtencionService.deleteDiaAtencion(this.diasAtencion.id);
      }
    })
    setTimeout(() => {
      this.comercio.deleteNegociobyId(comercio).subscribe(data => {
        this.comercios = this.comercios.filter(u => u !== comercio);
      })
  }, 2000);
    Swal.fire(
      'Eliminado!',
      'El comercio ha sido eliminado.',
      'success'
    )
  }
}) 
}
//validaciones

get nombreUsuario() {
  return this.registerForm.get('nombreUsuario');
}
get passwordActual() {
  return this.registerForm.get('passwordActual');
}
get passwordNueva() {
  return this.registerForm.get('passwordNueva');
}
get passwordConfirmacion() {
  return this.registerForm.get('passwordConfirmacion');
}
get nombre() {
  return this.registerForm.get('nombre');
}
get apellidoPaterno() {
  return this.registerForm.get('apellidoPaterno');
}
get apellidoMaterno() {
  return this.registerForm.get('apellidoMaterno');
}
get telefono() {
  return this.registerForm.get('telefono');
}
get correo() {
  return this.registerForm.get('correo');
}
registerForm = this.formBuilder.group({
  passwordActual:  ['', {
    validators: [Validators.required, Validators.minLength(4)],
    updateOn: 'blur'
  }],
  passwordNueva:  ['', {
    validators: [Validators.required, Validators.minLength(4)],
    updateOn: 'blur'
  }],
  passwordConfirmacion:  ['', {
    validators: [Validators.required, Validators.minLength(4)],
    updateOn: 'blur'
  }],
  nombre:  ['', {
    validators: [Validators.required],
    updateOn: 'blur'
  }],
  apellidoPaterno:  ['', {
    validators: [Validators.required],
    updateOn: 'blur'
  }],
  apellidoMaterno:  ['', {
    validators: [Validators.required],
    updateOn: 'blur'
  }],
  telefono:  ['', {
    validators: [Validators.required],
    updateOn: 'blur'
  }],
  correo:  ['', {
    validators: [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")],
    updateOn: 'blur'
  }],
});

}