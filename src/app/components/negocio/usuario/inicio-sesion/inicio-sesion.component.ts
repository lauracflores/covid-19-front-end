import { Component, OnInit } from '@angular/core';
import { usuario } from 'src/app/models/negocio/usuario/usuario.model';
import { NgForm, FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/negocio/usuario/login/auth.service';
import { Router} from '@angular/router'
import Swal from 'sweetalert2';

@Component({
  selector: 'app-inicio-sesion',
  templateUrl: './inicio-sesion.component.html',
  styleUrls: ['./inicio-sesion.component.css'],
  styles: [`.ng-invalid.ng-touched:not(form) { border: 1px solid red; }`]
})
export class InicioSesionComponent implements OnInit {
  usuario: usuario = new usuario();
  
  constructor(private auth: AuthService, private router: Router) { }

  ngOnInit(): void {
  }

  login(form: NgForm){
    if(this.usuario.nombreUsuario == undefined){
      this.alerta("Debes llenar todos los campos.");
      return;
    }else if(this.usuario.nombreUsuario.length < 1){
      this.alerta("Debes llenar todos los campos.");
      return;
    }
    this.auth.login(this.usuario)
    .subscribe(resp=>{
      this.router.navigateByUrl('/perfil');
    },(err)=>{
      if(err.status==404){
        this.alerta("Usuario o contraseña incorrecta");
      }
      else{
        this.alerta("Ocurrió un error, intenta mmás tarde");
      }
    });
  }

  alerta(texto){
    Swal.fire({
      icon: 'error',
      title: 'Oops...',
      text: texto,
    });
    this.cambiaBorde();
  }

  checaUsuario(){
    var usuario = document.getElementById("email");
    usuario.style.border = "";
    usuario.style.borderColor = "";
  }

  checaPassword(){
    var usuario = document.getElementById("password");
    usuario.style.border = "";
    usuario.style.borderColor = "";
  }

  cambiaBorde(){
    var usuario = document.getElementById("email");
    usuario.style.border = "1px solid";
    usuario.style.borderColor = "red";
    var usuario = document.getElementById("password");
    usuario.style.border = "1px solid";
    usuario.style.borderColor = "red";
  }

}
