import { Directive, Injectable } from '@angular/core';
import { NG_ASYNC_VALIDATORS, AsyncValidator } from '@angular/forms';
import { UsuarioService } from '../../../../services/negocio/usuario/registro/usuario.service';
import {map} from 'rxjs/operators';

@Directive({
  selector: '[usernameUnico]',
  providers: [{  provide: NG_ASYNC_VALIDATORS, useExisting: UsernameValidator, multi: true }]
})
export class UsernameValidator implements AsyncValidator{

  constructor(public service: UsuarioService) { }


  validate(control: import("@angular/forms").AbstractControl): Promise<import("@angular/forms").ValidationErrors> | import("rxjs").Observable<import("@angular/forms").ValidationErrors> {
    const username = control.value;
    const obs= this.service.getUsuarioByUsuario(username).pipe(
      map(usernameArr => {
        if (usernameArr['nombreUsuario']==username){
          return {usernameUnico: true};
        }
        return {usernameUnico: false};
        
      }
    ));
    return obs;

   /* obs.subscribe(
      () => {
        fail('expected error');
      },
      (error) => {
        console.log('Error');
        return null;
        expect(error).toBe('user already exist in database');
      })
      return obs;¨*/

 
  }

}

@Injectable({providedIn: 'root'})
export class UsernameUnicoService implements AsyncValidator  {
 constructor(public service: UsuarioService) { }

 validate(control: import("@angular/forms").AbstractControl): Promise<import("@angular/forms").ValidationErrors> | import("rxjs").Observable<import("@angular/forms").ValidationErrors>
 {
   const usernameUnicoDirective = new UsernameValidator(this.service);
    return usernameUnicoDirective.validate(control);
 }
}

