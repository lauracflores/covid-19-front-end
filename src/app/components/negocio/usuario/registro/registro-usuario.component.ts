import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import {UsernameUnicoService} from './username-validator.directive';
import {EmailUnicoService} from './email-validator.directive';
import { UsuarioService } from '../../../../services/negocio/usuario/registro/usuario.service';
import { usuario } from '../../../../models/negocio/usuario/usuario.model';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-registro-usuario',
  templateUrl: './registro-usuario.component.html',
  styleUrls: ['./registro-usuario.component.css']
})
export class RegistroUsuarioComponent implements OnInit {
  usuario: usuario = new usuario();
  constructor(private router: Router, private formBuilder: FormBuilder,
    private usernameUnicoService: UsernameUnicoService, private service: UsuarioService, private emailUnicoService: EmailUnicoService) {}
    guardar(){
      if(this.registerForm.get('nombreUsuario').status==='PENDING' && this.registerForm.get('password').status==='VALID' && this.registerForm.get('passwordConfirmacion').status==='VALID' && this.registerForm.get('nombre').status==='VALID' && this.registerForm.get('apellidoPaterno').status==='VALID' && this.registerForm.get('apellidoMaterno').status==='VALID' && this.registerForm.get('telefono').status==='VALID' && this.registerForm.get('correo').status==='PENDING'){
         this.service.postusuario(this.usuario).subscribe(data => {
          Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Usuario registrado correctamente',
            showConfirmButton: false,
            timer: 1500
          })
          this.router.navigate(["sesion"]);
        },(err)=>{
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Ocurrió un error, intenta más tarde',
          });
      });
      }
      else{
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Alguna regla de validación no se está cumpliendo',
        });
      }
  }

  get nombreUsuario() {
    return this.registerForm.get('nombreUsuario');
  }
  get password() {
    return this.registerForm.get('password');
  }
  get passwordConfirmacion() {
    return this.registerForm.get('passwordConfirmacion');
  }
  get nombre() {
    return this.registerForm.get('nombre');
  }
  get apellidoPaterno() {
    return this.registerForm.get('apellidoPaterno');
  }
  get apellidoMaterno() {
    return this.registerForm.get('apellidoMaterno');
  }
  get telefono() {
    return this.registerForm.get('telefono');
  }
  get correo() {
    return this.registerForm.get('correo');
  }
  registerForm = this.formBuilder.group({
    nombreUsuario: ['', {
      validators: [Validators.required],
      asyncValidators: [this.usernameUnicoService.validate.bind(this.usernameUnicoService)],
      updateOn: 'blur'
    }],
    password:  ['', {
      validators: [Validators.required, Validators.minLength(4)],
      updateOn: 'blur'
    }],
    passwordConfirmacion:  ['', {
      validators: [Validators.required],
      updateOn: 'blur'
    }],
    nombre:  ['', {
      validators: [Validators.required],
      updateOn: 'blur'
    }],
    apellidoPaterno:  ['', {
      validators: [Validators.required],
      updateOn: 'blur'
    }],
    apellidoMaterno:  ['', {
      validators: [Validators.required],
      updateOn: 'blur'
    }],
    telefono:  ['', {
      validators: [Validators.required],
      updateOn: 'blur'
    }],
    correo:  ['', {
      validators: [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")],
      asyncValidators: [this.emailUnicoService.validate.bind(this.emailUnicoService)],
      updateOn: 'blur'
    }],
  });

  ngOnInit() {
   
  }

}
