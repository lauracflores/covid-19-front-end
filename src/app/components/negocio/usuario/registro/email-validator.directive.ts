import { Directive, Injectable } from '@angular/core';
import { NG_ASYNC_VALIDATORS, AsyncValidator } from '@angular/forms';
import { UsuarioService } from '../../../../services/negocio/usuario/registro/usuario.service';
import {map} from 'rxjs/operators';

@Directive({
  selector: '[emailUnico]',
  providers: [{  provide: NG_ASYNC_VALIDATORS, useExisting: EmailValidator, multi: true }]
})
export class EmailValidator implements AsyncValidator{

  constructor(public service: UsuarioService) { }


  validate(control: import("@angular/forms").AbstractControl): Promise<import("@angular/forms").ValidationErrors> | import("rxjs").Observable<import("@angular/forms").ValidationErrors> {
    const email = control.value;
    const obs= this.service.getUsuarioByEmail(email).pipe(
      map(EmailArr => {
        if (EmailArr['correo']==email){
          return {emailUnico: true};
        }
        return {emailUnico: false};
        
      }
    ));
    return obs;
  }
}

@Injectable({providedIn: 'root'})
export class EmailUnicoService implements AsyncValidator  {
 constructor(public service: UsuarioService) { }

 validate(control: import("@angular/forms").AbstractControl): Promise<import("@angular/forms").ValidationErrors> | import("rxjs").Observable<import("@angular/forms").ValidationErrors>
 {
   const emailUnicoDirective = new EmailValidator(this.service);
    return emailUnicoDirective.validate(control);
 }
}

