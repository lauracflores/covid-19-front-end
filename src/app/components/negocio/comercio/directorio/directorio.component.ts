import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { DirectorioService} from '../../../../services/negocio/comercio/directorio/directorio.service';
import { Comercio} from '../../../../models/negocio/comercio/comercio.model';
import {MatTableDataSource} from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';

import {AgmMap, MouseEvent,MapsAPILoader  } from '@agm/core';


@Component({
  selector: 'app-directorio',
  templateUrl: './directorio.component.html',
  styleUrls: ['./directorio.component.css']
})
export class DirectorioComponent implements OnInit {

  negocios: Comercio[];
  displayedColumns: string[] = ['nombre', 'telefono', 'domicilio','sitioWeb','horario'];
  currLat:number;
  currLng:number;
  dataSource: MatTableDataSource<Comercio>
  dataSourceEmpty = []
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  constructor(private service: DirectorioService, private router: Router,private apiloader: MapsAPILoader) { }
  
  ngOnInit(): void {
    this.getPosition().then(pos=>
      {
         console.log(`Positon: ${pos.lng} ${pos.lat}`);
      
    this.service.getNegocioByRadio(pos.lng,pos.lat).subscribe(Response=>{
    this.negocios=Response;   
    this.dataSource=new MatTableDataSource(this.negocios)
    this.dataSource.paginator = this.paginator;
     
    })
  });
  }
 
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    console.log(this.negocios[0].nombre);

  }

getCurrentLocation() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(position => {

      this.currLat = position.coords.latitude;
      this.currLng = position.coords.longitude;
    });
  }
  else {
    alert("Geolocation is not supported by this browser.");
  }
}
getPosition(): Promise<any>
  {
    return new Promise((resolve, reject) => {

      navigator.geolocation.getCurrentPosition(resp => {

          resolve({lng: resp.coords.longitude, lat: resp.coords.latitude});
        },
        err => {
          reject(err);
        });
    });

  }
}
