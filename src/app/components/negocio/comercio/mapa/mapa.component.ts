import { Component, OnInit, ViewChild } from '@angular/core';
import { MapaService } from 'src/app/services/negocio/comercio/mapa/mapa.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import * as L from 'leaflet';

export interface Negocio {
  nombre: String,
  calle: String, 
  numExt: String, 
  numInt: String,
  colonia: String, 
  municipio: String,
  estado: String, 
  codigoPostal: String,
  telefono: String,
}

var redIcon,blueIcon,LeafIcon;
var lat,lng;
var places;
var map;
var array;

@Component({
selector: 'app-mapa',
templateUrl: './mapa.component.html',
styleUrls: ['./mapa.component.css']
})

export class MapaComponent implements OnInit {
    
  public negocios;
  comercio;
  displayedColumns: string[] = ['Nombre', 'Direccion', 'Telefono'];
  data: Negocio[];
  dataSource: MatTableDataSource<Negocio>
  dataSourceEmpty = []
  currLat: number;
  currLng: number;
  showGrid = false;

  radios=[{
  radio: 200
  },{
  radio: 300
  },{
  radio: 400
  },{
  radio: 500
  },{
  radio: 600
  },{
  radio: 700
  },{
  radio: 800
  },{
  radio: 900
  },{
  radio: 1000
  }
]

@ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
constructor(private service: MapaService) { }

    ngOnInit(): void {
        var ruta = '../../../../../assets/components/negocio/comercio/mapa/leafleticons/';
        map = L.map("map").setView([21.882969, -102.293025],14);
        map.addEventListener('click',event=>{
          lat = event.latlng.lat;
          lng = event.latlng.lng;
          this.deleteLayers();
          L.marker([lat,lng],{icon: redIcon}).addTo(map);
        });
        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);
        LeafIcon = L.Icon.extend({
          options: {
            shadowUrl: ruta+'marker-shadow.png',
            iconSize: [25, 41],
            iconAnchor: [12, 41],
            popupAnchor: [1, -34],
            tooltipAnchor: [16, -28],
            shadowSize: [41, 41]
          }
        });
        blueIcon = new LeafIcon({iconUrl: ruta+'marker-icon.png'}),
        redIcon = new LeafIcon({iconUrl: ruta+'red.png'});
      }

      consultaNegocio(event) {
        console.log(this.currLat);
        array.forEach(negocio =>{
          if(negocio.id==event.layer.id){
            console.log(negocio);
            this.comercio = negocio;
          }
        });
      }

      deleteLayers(){
        var cont = 0;
        array = [];
        this.showGrid = false;
        this.dataSource = new MatTableDataSource();
        map.eachLayer(function (layer) {
          cont ++ ;
          if(cont > 1){
            map.removeLayer(layer);
          }
        });
      }

      buscaNegocios(radio){
        if(lat == undefined){
          alert("primero debes seleccionar un punto en el mapa");
          return
        }
        this.buscaNegocio(lat,lng,radio);
      }
      
      async buscaNegocio(lat,lng,rad){
        const res = await this.service.buscaNegocios(lat,lng,rad);
        if(places != undefined){
          this.deleteLayers();
        }

        this.service.buscaComercios(lat,lng,rad).subscribe(Response =>{
          this.data = Response;
          this.dataSource = new MatTableDataSource(this.data);
          this.dataSource.paginator = this.paginator;
        })
        L.marker([lat,lng],{icon: redIcon}).addTo(map);
        L.circle([lat, lng], {radius: (rad)}).addTo(map);
        array = res;
        if(array.length > 0){
          this.showGrid = true;
        }
        array.forEach((negocio) => {
          places = L.marker([negocio['latitud'],negocio['longitud']],{icon: blueIcon}).addTo(L.featureGroup().addTo(map).addEventListener("click", this.consultaNegocio)).bindPopup(negocio['nombre']);//.openPopup();
          places.nombre = negocio['nombre']
          places.id = negocio['id'];
        });
      }
      
      applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.dataSource.filter = filterValue.trim().toLowerCase();
      }

  }