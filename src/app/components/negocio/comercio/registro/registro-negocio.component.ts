import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Comercio } from 'src/app/models/negocio/comercio/comercio.model';
import { Router } from '@angular/router';
import { AltaNegocioService } from 'src/app/services/negocio/comercio/alta-negocio/alta.negocio.service';
import { entidades } from './entidades';
import { municipios } from './municipios';
import { codEntidad } from './cod.entidad';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-registro-negocio',
  templateUrl: './registro-negocio.component.html',
  styleUrls: ['./registro-negocio.component.css']
})
export class RegistroNegocioComponent implements OnInit {
  panelOpenState = false;
  step = 0;

  setStep(index: number) {
    this.step = index;
  }

  nextStep() {
    this.step++;
  }

  prevStep() {
    this.step--;
  }

  loading = false;
  negocio: Comercio = new Comercio();
  negocios: Comercio[];
  entidades;
  municipios;
  codEntidad;
  show = false;
  showNegocio = false;
  comercio;

  constructor(private service: AltaNegocioService, private router: Router) { }

  ngOnInit(): void {
    this.entidades = entidades;
    this.negocio.estado = 'AGUASCALIENTES';
    this.municipios = municipios['AGUASCALIENTES'];
    this.negocio.municipio = 'Aguascalientes';
    this.codEntidad = 1;
  }

  cargaMunicipios(entidad){
    try{
      this.municipios = municipios[entidad];
      this.negocio.municipio = this.municipios[0];
      this.codEntidad = codEntidad[entidad];
    }catch(error){
      console.log("no carga");
    }
  }

  verificar(form: NgForm){
    this.loading = true;
    this.service.verificaNegocio(this.negocio.nombre,this.codEntidad,this.negocio.municipio)
    .subscribe((res: Comercio[])=> {
      if(res.length > 0){
        this.negocios = res;
        this.comercio = this.negocios[0];
        this.show = true;
        this.loading = false;
        this.step = 1;
      }else{
        this.loading = false;
        this.alerta('No se encuentra ningun establecimiento con ese nombre o razón social, por favor verifica tus datos y vuelve a intentar.');
      }
    },(err)=>{
      this.loading = false;
      this.alerta('No se encuentra ningun establecimiento con ese nombre o razón social, por favor verifica tus datos y vuelve a intentar.');
    });
  }

  cargaNegocio(nombreNegocio){
    this.negocios.forEach(negocio =>{
      if(nombreNegocio == negocio['Nombre']){
        this.showNegocio = true;
        this.comercio = negocio;
      }
    });
  }

  registrar(form: NgForm){
    if(String(this.comercio.Telefono).length > 9){
      this.service.registraNegocio(this.comercio);
    }else{
      this.alerta("Ingresa un numero de telefono válido");
    }
  }

  alerta(texto){
    Swal.fire({
      icon: 'error',
      title: 'Oops...',
      text: texto,
    });
  }

  checaTelefono(telefono){
    var tel = document.getElementById("telefono");
    if(telefono.length > 0 && telefono.length < 10){
      tel.style.border = "1px solid";
      tel.style.borderColor = "red";
    }else{
      tel.style.border = "";
      tel.style.borderColor = "";
    }
  }

}