import { Component, OnInit } from '@angular/core';
import { DiasAtencionService } from 'src/app/services/negocio/comercio/registro/dias-atencion.service';
import { usuario } from 'src/app/models/negocio/usuario/usuario.model';
import { DiasAtencion } from 'src/app/models/negocio/comercio/dias-atencion.model';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registro-negocio-horario',
  templateUrl: './registro-negocio-horario.component.html',
  styleUrls: ['./registro-negocio-horario.component.css']
})
export class RegistroNegocioHorarioComponent implements OnInit {
  diasAtencion: DiasAtencion = new DiasAtencion();
  usuario: usuario = new usuario();
  aux: string;
  diasLaborales: DiasAtencion[];
  horario = ['00:00','00:30','01:00','01:30','02:00','03:00','03:30','04:00','04:30','05:00','05:30','06:00', '06:30', '07:00', '07:30','08:00','08:30','09:00','09:30','10:00','10:30','11:00','11:30','12:00','12:30','13:00','13:30','14:00','14:30','15:00','15:30','16:00','16:30','17:00','17:30','18:00','18:30','19:00','19:30','20:00','20:30','21:00','21:30','22:00','22:30','23:00','23:30'];
  dias = ['Lunes','Martes','Miercoles','Jueves','Viernes','Sabado','Domingo'];
  desabilitar=[false, true, true, true, true, true, true];
  constructor(private diasAtencionService: DiasAtencionService, private router: Router) { }

  ngOnInit(): void {
    var cont = 0;
    this.diasAtencionService.getHorario2(Number(localStorage.getItem("idComercio"))).subscribe(resp=>{
        this.diasLaborales = resp;
        this.diasLaborales.forEach(dia=>{
          this.desabilitar[cont]=true;
          cont++;
          this.diasAtencion.dia = this.dias[cont]; 
          this.desabilitar[cont]=false;
        });
    },err=>{
      this.diasAtencion.dia='Lunes';
    });
    console.log(this.diasAtencion);
    this.diasAtencion.horaApertura='08:00';
    this.diasAtencion.horaCierre='17:00';
    this.diasAtencion.abierto=1;

  }
  updateAbierto(event) {
    if(this.diasAtencion.abierto===1){
      this.diasAtencion.horaApertura='08:00';
      this.diasAtencion.horaCierre='17:00';
    }
    else{
      this.diasAtencion.horaApertura='';
      this.diasAtencion.horaCierre='';
    }
    
  }
  guardar()
  {
    console.log(this.diasAtencion.negocio=localStorage['userbyuser']);
    this.diasAtencion.negocio= parseInt(localStorage.getItem('idComercio'));
    this.aux="1970-01-01T".concat(this.diasAtencion.horaApertura).concat(":00-06:00");
    this.diasAtencion.horaApertura=this.aux;
    this.aux="1970-01-01T".concat(this.diasAtencion.horaCierre).concat(":00-06:00");
    this.diasAtencion.horaCierre=this.aux;
    if(this.diasAtencion.abierto===0){
      this.diasAtencion.horaApertura=null;
      this.diasAtencion.horaCierre=null;
    }
    this.diasAtencionService.postDiaAtencion(this.diasAtencion);
    this.diasAtencion.abierto=1;
    this.diasAtencion.horaApertura='08:00';
    this.diasAtencion.horaCierre='17:00';
    switch(this.diasAtencion.dia) { 
      case 'Lunes': { 
        this.desabilitar[0]=true;
        this.desabilitar[1]=false;
        this.diasAtencion.dia='Martes';        
         break; 
      } 
      case 'Martes': { 
         this.desabilitar[1]=true;
         this.desabilitar[2]=false;
         this.diasAtencion.dia='Miercoles';
         break; 
      } 
      case 'Miercoles': { 
        this.desabilitar[2]=true;
        this.desabilitar[3]=false;
        this.diasAtencion.dia='Jueves';
        break; 
       } 
       case 'Jueves': { 
        this.desabilitar[3]=true;
        this.desabilitar[4]=false;
        this.diasAtencion.dia='Viernes';
        break; 
       } 
       case 'Viernes': { 
        this.desabilitar[4]=true;
        this.desabilitar[5]=false;
        this.diasAtencion.dia='Sabado';
        break; 
       } 
       case 'Sabado': { 
        this.desabilitar[5]=true;
        this.desabilitar[6]=false;
        this.diasAtencion.dia='Domingo';
        break; 
       } 
        case 'Domingo': { 
          this.desabilitar[6]=true;  
          Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Horario completo registrado',
            showConfirmButton: false,
            timer: 1500
          })
          localStorage.setItem('idComercio','');
           this.router.navigate(["perfil"]);     
          break; 
        } 
        
      
       
   } 
  }

}
