import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistroNegocioHorarioComponent } from './registro-negocio-horario.component';

describe('RegistroNegocioHorarioComponent', () => {
  let component: RegistroNegocioHorarioComponent;
  let fixture: ComponentFixture<RegistroNegocioHorarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistroNegocioHorarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistroNegocioHorarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
