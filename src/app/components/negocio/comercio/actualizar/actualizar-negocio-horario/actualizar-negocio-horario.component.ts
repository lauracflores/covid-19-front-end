import { Component, OnInit } from '@angular/core';
import { DiasAtencionService } from 'src/app/services/negocio/comercio/registro/dias-atencion.service';
import { usuario } from 'src/app/models/negocio/usuario/usuario.model';
import { DiasAtencion } from 'src/app/models/negocio/comercio/dias-atencion.model';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { Comercio } from 'src/app/models/negocio/comercio/comercio.model';
import {EmailUnicoService} from '../../../usuario/registro/email-validator.directive';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-actualizar-negocio-horario',
  templateUrl: './actualizar-negocio-horario.component.html',
  styleUrls: ['./actualizar-negocio-horario.component.css']
})
export class ActualizarNegocioHorarioComponent implements OnInit {
  diasAtencion: DiasAtencion = new DiasAtencion();
  comercio:Comercio = new Comercio();
  diasAtencionArray: DiasAtencion[];
  usuario: usuario = new usuario();
  aux: string;
  dia: number;
  form: FormGroup;
  step = 0;
  horario = ['00:00','00:30','01:00','01:30','02:00','03:00','03:30','04:00','04:30','05:00','05:30','06:00', '06:30', '07:00', '07:30','08:00','08:30','09:00','09:30','10:00','10:30','11:00','11:30','12:00','12:30','13:00','13:30','14:00','14:30','15:00','15:30','16:00','16:30','17:00','17:30','18:00','18:30','19:00','19:30','20:00','20:30','21:00','21:30','22:00','22:30','23:00','23:30'];
  dias = ['Lunes','Martes','Miercoles','Jueves','Viernes','Sabado','Domingo'];
  constructor(private diasAtencionService: DiasAtencionService, private router: Router,private formBuilder: FormBuilder) { 
    this.form = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.pattern("[a-z0-9._-]+@([a-z0-9.-]+\.)[a-z]{3}$")]),
      telefono: new FormControl('', [Validators.required, Validators.pattern("[0-9]{10}")])
    });
  }

  setStep(index: number) {
    this.step = index;
  }

  nextStep() {
    this.step++;
  }

  prevStep() {
    this.step--;
  }
  ngOnInit(): void {
    this.diasAtencionService.getNegocioById().subscribe(res=>{
      this.comercio=res;
    });
    this.diasAtencionService.getHorario2(parseInt(localStorage.getItem('idComercio'))).subscribe(Response=>{
      if(Response.length < 7){
        this.router.navigate(["registro-horario"]);
      }
      this.diasAtencionArray=Response;  
      this.diasAtencion.dia=this.diasAtencionArray[0].dia;
      if(this.diasAtencionArray[0].horaApertura!='' && this.diasAtencionArray[0].horaCierre!=''){
        this.diasAtencion.horaApertura=this.diasAtencionArray[0].horaApertura?this.diasAtencionArray[0].horaApertura.split('1970-01-01T').join('-').split('-',2).join('').split(':',2).join(':'):''
        this.diasAtencion.horaCierre=this.diasAtencionArray[0].horaCierre?this.diasAtencionArray[0].horaCierre.split('1970-01-01T').join('-').split('-',2).join('').split(':',2).join(':'):''
      }else{
        this.diasAtencion.horaApertura=this.diasAtencionArray[0].horaApertura;
        this.diasAtencion.horaApertura=this.diasAtencionArray[0].horaApertura
      }
     
      this.diasAtencion.abierto=this.diasAtencionArray[0].abierto;
      },err=>{
        if(err.status == 404 ){
          this.router.navigate(["registro-horario"]);
        }
      });
  }

  updateAbierto(event) {
    if(this.diasAtencion.abierto===1){
      this.diasAtencion.horaApertura='08:00';
      this.diasAtencion.horaCierre='17:00';
    }
    else{
      this.diasAtencion.horaApertura='';
      this.diasAtencion.horaCierre='';
    }
    
  }
  updateDia(event) {
      switch(this.diasAtencion.dia) { 
        case 'Lunes': { 
         this.dia=0;
           break; 
        } 
        case 'Martes': { 
           this.dia=1;
           break; 
        } 
        case 'Miercoles': { 
          this.dia=2;
          break; 
         } 
         case 'Jueves': { 
          this.dia=3;
          break; 
         } 
         case 'Viernes': { 
          this.dia=4;
          break; 
         } 
         case 'Sabado': { 
          this.dia=5;
          break; 
         } 
          case 'Domingo': { 
            this.dia=6;
            break; 
          } 
          
        
         
     } 
   this.diasAtencion.dia=this.diasAtencionArray[this.dia].dia;
   this.diasAtencion.horaCierre=this.diasAtencionArray[this.dia].horaCierre?this.diasAtencionArray[this.dia].horaCierre.split('1970-01-01T').join('-').split('-',2).join('').split(':',2).join(':'):'';
   this.diasAtencion.horaApertura=this.diasAtencionArray[this.dia].horaApertura?this.diasAtencionArray[this.dia].horaApertura.split('1970-01-01T').join('-').split('-',2).join('').split(':',2).join(':'):'';
   this.diasAtencion.abierto=this.diasAtencionArray[this.dia].abierto;   
    
    }
    irAPerfil(){
      this.router.navigate(["perfil"]);     
    }
  guardar()
  {
    this.diasAtencion.negocio= parseInt(localStorage.getItem('idComercio'));
    if(this.diasAtencion.horaApertura!='' && this.diasAtencion.horaCierre!=''){
      this.aux="1970-01-01T".concat(this.diasAtencion.horaApertura).concat(":00-06:00");
      this.diasAtencion.horaApertura=this.aux;
      this.aux="1970-01-01T".concat(this.diasAtencion.horaCierre).concat(":00-06:00");
      this.diasAtencion.horaCierre=this.aux;
    }
   console.log('apertura['+this.diasAtencion.horaApertura+']')
    
  
    switch(this.diasAtencion.dia) { 
      case 'Lunes': { 
      this.diasAtencionService.putDiaAtencion(this.diasAtencion,this.diasAtencionArray[0].id);
         break; 
      } 
      case 'Martes': { 
        this.diasAtencionService.putDiaAtencion(this.diasAtencion,this.diasAtencionArray[1].id);
         break; 
      } 
      case 'Miercoles': { 
        this.diasAtencionService.putDiaAtencion(this.diasAtencion,this.diasAtencionArray[2].id);
        break; 
       } 
       case 'Jueves': { 
        this.diasAtencionService.putDiaAtencion(this.diasAtencion,this.diasAtencionArray[3].id);
        break; 
       } 
       case 'Viernes': { 
        this.diasAtencionService.putDiaAtencion(this.diasAtencion,this.diasAtencionArray[4].id);
        break; 
       } 
       case 'Sabado': { 
        this.diasAtencionService.putDiaAtencion(this.diasAtencion,this.diasAtencionArray[5].id);
        break; 
       }  
       
   }  
   
  }

  checaTelefono(telefono){
    var tel = document.getElementById("telefono");
    if(telefono.length > 0 && telefono.length < 10){
      tel.style.border = "1px solid";
      tel.style.borderColor = "red";
    }else{
      tel.style.border = "";
      tel.style.borderColor = "";
    }
  }

  actualizaNegocio(){
    if(!this.form.valid){
      this.alertaError("Debes llenar los campos correctamente.");
      return;
    }
    this.diasAtencionService.putNegocio(this.form.value).subscribe(response=>{
      this.alertaExito("Los datos se actualizaron correctamente.");
    },err =>{
      this.alertaError("Ocurrió un error inesperado, por favor intenta mas tarde.");
    });
  }

  alertaError(texto){
    Swal.fire({
      icon: 'error',
      title: 'Oops...',
      text: texto,
    });
  }

  alertaExito(texto){
    Swal.fire({
      icon: 'success',
      title: texto,
      showConfirmButton: false,
      timer: 1500
    });
  }

}

