import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActualizarNegocioHorarioComponent } from './actualizar-negocio-horario.component';

describe('ActualizarNegocioHorarioComponent', () => {
  let component: ActualizarNegocioHorarioComponent;
  let fixture: ComponentFixture<ActualizarNegocioHorarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActualizarNegocioHorarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActualizarNegocioHorarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
