import { Component, OnInit  } from '@angular/core';
import {GraficasGobiernoService} from '../../../services/estadisticas/graficas-gobierno/graficas-gobierno.service';


@Component({
    selector: 'app-graficas-gobierno',
    templateUrl: './graficas-gobierno.component.html',
    styleUrls: ['./graficas-gobierno.component.css']
})

export class GraficasGobiernoComponent implements OnInit{
    jsonCasos : any;
    JSON: Promise<void>;
    jsonComorbilidad : any;
    jsonTimeline : any;


    //Genero
    public pieChartLabels = ["Mujeres","Hombres"];
    public pieChartData = [1,1];
    public pieChartType = 'pie';

    //Confirmados
    public lineChartLabels = ["2","1","1","1"];
    public lineChartData = [{
        backgroundColor: 'rgba(0, 0, 0, 0.01)',
        borderColor: 'rgb(255, 99, 132)',
        data : [1,1,1,1],
        label: 'Muertes'
    }];    
    public lineChartType = 'line';

    //Sospechosos
    public lineChartLabelsSospechoso = ["2","1","1","1"];
    public lineChartDataSospechoso = [{
        backgroundColor: 'rgba(0, 0, 0, 0.01)',
        borderColor: 'rgb(255, 99, 132)',
        data : [1,1,1,1],
        label: 'Muertes'
    }];    
    public lineChartTypeSospechoso = 'line';

    //Muertes
    public lineChartLabelsMuertes = ["2","1","1","1"];
    public lineChartDataMuertes = [{
        backgroundColor: 'rgba(0, 0, 0, 0.01)',
        borderColor: 'rgb(255, 99, 132)',
        data : [1,1,1,1],
        label: 'Muertes'
    }];    
    public lineChartTypeMuertes = 'line';

    Muertes

    //Por enfermedad
    public barChartData = [{
        backgroundColor: 'rgba(220, 1, 1)',
        borderColor: 'rgb(255, 99, 132)',
        data : [1,1,1,1,1,1,1,1,1],
        label: 'Muertes'
    }];
    public barChartLabels = ["Epoc","Hipertension","Neumonia","Inmunusuprimido","Renalcronica","Asma","Obesidad","Cardiovascular","Diabetes"];
    public barChartOptions = [];
    public barChartType = 'horizontalBar';


    constructor(private graficasGobiernoService: GraficasGobiernoService) { }
    ngOnInit() {
        
    };

    onChange(state) {
        this.changeEstate(state);
    }
    
    private changeEstate(state: string){
        this.changePieChart(state);
        this.changeBarChart(state);
        this.changeConfirmedChart(state);
        this.changeSuspectsChart(state);
        this.changeDeathsChart(state);
    }

    changePieChart(state:string){
        this.JSON = this.graficasGobiernoService.getCasosByState(state).toPromise()
        .then((res) => {
            this.jsonCasos = res;
            this.pieChartData = [this.jsonCasos.sexoConfirmados.hombres,this.jsonCasos.sexoConfirmados.mujeres];
        });
    }

    changeBarChart(state:string){
        this.JSON = this.graficasGobiernoService.getMuertesByEnfermedad(state).toPromise()
        .then((res) => {
            this.jsonComorbilidad = res;
            let epoc = this.jsonComorbilidad.tipo.epoc;
            let hipertension = this.jsonComorbilidad.tipo.hipertension;
            let neumonia = this.jsonComorbilidad.tipo.neumonia;
            let inmunusuprimido = this.jsonComorbilidad.tipo.inmunusuprimido;
            let renalCronica = this.jsonComorbilidad.tipo.renalCronica;
            let obesidad = this.jsonComorbilidad.tipo.obesidad;
            let asma = this.jsonComorbilidad.tipo.asma;
            let cardiovascular = this.jsonComorbilidad.tipo.cardiovascular;
            let diabetes = this.jsonComorbilidad.tipo.diabetes;

            this.barChartData = [{
                backgroundColor: 'rgba(220, 1, 1)',
                borderColor: 'rgb(255, 99, 132)',
                data : [epoc,hipertension,neumonia,inmunusuprimido,renalCronica,obesidad,asma,cardiovascular,diabetes],
                label: 'Resumen de muertes por Comorbilidad'
            }];
        });
    }

    changeConfirmedChart(state: string){
        this.jsonCasos = this.graficasGobiernoService.getTimeline(state,"confirmed").subscribe((res) =>{
            this.jsonTimeline = res;

            let fechas: Array<string> = [];
            let casos: Array<string> = [];

            for (let index = 0; index < this.jsonTimeline.length; index++) {
                fechas.push(this.jsonTimeline[index].fecha);
                casos.push(this.jsonTimeline[index].casos);
            }

            let fechaDos: any[];
            fechaDos = fechas;

            let casosDos: any[];
            casosDos = casos;

            this.lineChartLabels = fechaDos;

            this.lineChartData = [{
                backgroundColor: 'rgba(0, 0, 0, 0.01)',
                borderColor: 'rgb(16, 123, 255)',
                data : casosDos,
                label: 'Confirmados'
            }];
           
        })
    }

    changeSuspectsChart(state: string){
        this.jsonCasos = this.graficasGobiernoService.getTimeline(state,"suspects").subscribe((res) =>{
            this.jsonTimeline = res;

            let fechas: Array<string> = [];
            let casos: Array<string> = [];

            for (let index = 0; index < this.jsonTimeline.length; index++) {
                fechas.push(this.jsonTimeline[index].fecha);
                casos.push(this.jsonTimeline[index].casos);
            }

            let fechaDos: any[];
            fechaDos = fechas;

            let casosDos: any[];
            casosDos = casos;

            this.lineChartLabelsSospechoso = fechaDos;

            this.lineChartDataSospechoso = [{
                backgroundColor: 'rgba(0, 0, 0, 0.01)',
                borderColor: 'rgb(255, 193, 7)',
                data : casosDos,
                label: 'Sospechosos'
            }];
           
        })
    }

    changeDeathsChart(state: string){
        this.jsonCasos = this.graficasGobiernoService.getTimeline(state,"deaths").subscribe((res) =>{
            this.jsonTimeline = res;

            let fechas: Array<string> = [];
            let casos: Array<string> = [];

            for (let index = 0; index < this.jsonTimeline.length; index++) {
                fechas.push(this.jsonTimeline[index].fecha);
                casos.push(this.jsonTimeline[index].casos);
            }

            let fechaDos: any[];
            fechaDos = fechas;

            let casosDos: any[];
            casosDos = casos;

            this.lineChartLabelsMuertes = fechaDos;

            this.lineChartDataMuertes = [{
                backgroundColor: 'rgba(0, 0, 0, 0.01)',
                borderColor: 'rgb(0, 0, 0)',
                data : casosDos,
                label: 'Muertes'
            }];
           
        })
    }

   
}