import { Component, OnInit } from '@angular/core';
import { StatisticsFilterService } from '../../../services/estadisticas/casos-mapa/statisticsfilter.service';

declare var paintMap: any;

@Component({
  selector: 'app-casos-covid',
  templateUrl: './casos-mapa.component.html',
  styleUrls: ['./casos-mapa.component.css']
})
export class CasosMapaComponent implements OnInit {

  statisticsJSON: any;

  constructor(private statisticsFilterService: StatisticsFilterService) { }

  ngOnInit(): void {
    this.statisticsJSON = this.statisticsFilterService.getMexicoStatistics().subscribe(resp => {
      paintMap(resp);
    });
  }

}
