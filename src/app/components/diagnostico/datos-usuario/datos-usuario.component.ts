import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NgForm } from '@angular/forms';

const DATA_NOT_VALID =
  'Información incompleta, profavor llene el formulario cuidadosamente';

@Component({
    selector: 'datos-usuario',
    templateUrl: './datos-usuario.component.html',
    styleUrls: ['./datos-usuario.component.css']
})

export class DatosDiagnosticoComponent {
  @Input() gender: string = '';
  @Output() genderChange = new EventEmitter<string>();
  @Input() age: number = 0;
  @Output() ageChange = new EventEmitter<number>();
  
  errorMsg: string;
  
  constructor() {}

  ngOnInit() {}

  onSelectGender(gender: string) {
    this.gender = gender;
  }

  onSubmit = (form: NgForm) => {
    console.log('gender ', this.gender, ' age: ', this.age);
    if (this.gender != '' && this.age >= 0) {
        this.ageChange.emit(this.age);
        this.genderChange.emit(this.gender);
    } else {
      this.errorMsg = DATA_NOT_VALID;
    }
  };
}