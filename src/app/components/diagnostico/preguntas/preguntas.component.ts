import { Component, OnInit, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Pregunta } from 'src/app/models/diagnostico/preguntas/pregunta.model';
import { PreguntaService } from 'src/app/services/diagnostico/preguntas/pregunta.service';

const DATA_NOT_VALID =
  'Información mal seleccionada, porfavor intentalo de nuevo cuidadosamente';

@Component({
  selector: 'preguntas-diagnostico',
  templateUrl: './preguntas.component.html',
  styleUrls: ['./preguntas.component.css'],
})
export class PreguntasComponent implements OnInit {
  @Input() gender: string;
  @Input() age: number;
  @Output() evidence = new EventEmitter<Object[]>();

  resp: Pregunta;
  errorMsg: string;
  map = new Map<string, string>();
  isRequesting = false;
  isSignedUp = false;

  constructor(
    private diagnosisService: PreguntaService
  ) {}

  ngOnInit() {
    this.setDiagnosis(this.gender, this.age, []);
  }

  onSelectGender(gender: string) {
    this.gender = gender;
  }

  private setDiagnosis = (
    gender: string,
    age: number,
    evidence: Object[]
  ): void => {
    //console.log(evidence);
    this.isRequesting = true;

    //console.log(this.map);
    this.diagnosisService.postDiagnosis(gender, age, evidence).subscribe(
      (resp) => {
        this.isRequesting = false;
        this.errorMsg = null;
        // print
        console.log(resp);
        this.resp = resp;
        if (this.resp['question']) {
          this.resp.question.items.forEach((item) => {
            this.map.set(item.id, '');
          });
        }
      },
      (err) => {},
      () => {
        if (this.resp.should_stop) {
          this.evidence.emit(evidence)
        }
      }
    );
  };

  private validateForm = (items : any)=>{
    console.log(items)
    let times:number=0;
    items.forEach((item)=>{
      if(this.map.get(item.id) ==''){
        times++;
      }
    })
    this.errorMsg= times>0 ? DATA_NOT_VALID : '';
    return times>0 ? false : true;
  };

  onSubmit = (form: NgForm) => {
    let evidence: Object[] = [];
    this.map.forEach((v, k) => {
      if (this.resp.question.type == 'group_single') {
        let id = form.value['group_single'];
        if (id == '') {
          this.errorMsg = DATA_NOT_VALID;
        } else {
          v = k == id ? 'present' : 'absent';
        }
      } else {
        let value = form.value[k];
        if (value) {
          v = value;
        }
      }
      this.map.set(k, v);
      evidence.push({ id: k, choice_id: v });
    });
    if(this.validateForm(this.resp.question.items))
      this.setDiagnosis(this.gender, this.age, evidence);
  }
}
