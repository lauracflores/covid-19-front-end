import { Component, OnInit, OnChanges } from '@angular/core';

@Component({
  selector: 'app-diagnostico',
  templateUrl: './diagnostico.component.html',
  styleUrls: ['./diagnostico.component.css']
})
export class DiagnosticoComponent implements OnInit, OnChanges {
  gender: string = '';
  age: number = 0;
  evidence: Object[];

  constructor() { }

  ngOnInit(): void {}

  ngOnChanges(): void {
    console.log(this.gender,this.age,this.evidence);
  }

  getEvidence = (evidence: Object[]) => {
    this.evidence = evidence;
  }
}
