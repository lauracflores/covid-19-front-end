import { Component, OnInit, Input } from '@angular/core';
import { ResultadoService } from 'src/app/services/diagnostico/resultado/resultado.service';
import { Resultado } from 'src/app/models/diagnostico/resultado/resultado.model';

@Component({
  selector: 'resultado',
  templateUrl: './resultado.component.html',
  styleUrls: ['./resultado.component.css']
})
export class ResultadoComponent implements OnInit {
  @Input() evidence: Object[];
  @Input() gender: string;
  @Input() age: number;
  triage: Resultado;

  constructor(
    private triageService: ResultadoService
  ) { }

  ngOnInit(): void {
    this.setTriage(this.gender, this.age, this.evidence);
  }
  
  private setTriage = (
    gender: string,
    age: number,
    evidence: Object[]
  ): void => {
    this.triageService.postTriage(gender, age, evidence).subscribe((triage) => {
      console.log('Respuesta: ', triage);
      this.triage = triage;
    });
  };

}
