import { Component } from '@angular/core';
import { Subject } from 'rxjs';
import { YoutubeService } from '../../../services/informativos/videos/youtube.service';
import { takeUntil } from 'rxjs/operators';
import { DomSanitizer } from '@angular/platform-browser';
/*import { $ } from 'protractor';*/

@Component({
  selector: 'app-videos',
  templateUrl: './videos.component.html',
  styleUrls: ['./videos.component.css']
})
export class VideosComponent {
videos: any[];
private unsubscribe$: Subject<any> = new Subject();

constructor(private youTubeService: YoutubeService, private sanitizer: DomSanitizer) { }

  getEmbedUrl(video){
    return this.sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/' + video.id.videoId);
  }

  ngOnInit() {
    this.videos = [];
    this.youTubeService
      .getVideosForChanel('UCu2Uc7YeJmE9mvGG9OK-zbQ', 6)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(lista => {
        for (let element of lista["items"]) {
          this.videos.push(element)
        }

      });
  }
/*
  cerrarModal(){
    this.videos=null;
    $('#exampleModal').modal('hide');
  }
*/
}
