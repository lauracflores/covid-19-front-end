import { Component, OnInit } from '@angular/core';
import { NewsApiService } from '../../../services/informativos/blogs/news-api.service';


@Component({
  selector: 'app-blogs-informativos',
  templateUrl: './blogs-informativos.component.html',
  styleUrls: ['./blogs-informativos.component.css']
})
export class BlogsInformativosComponent implements OnInit {

mArticles:Array<any>;

constructor(private newsapi:NewsApiService){
		console.log('app component constructor called');
	}

	ngOnInit() {
        //load articles
	    this.newsapi.initArticles().subscribe(data => this.mArticles = data['articles']);
    }




}
