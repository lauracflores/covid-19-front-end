import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from '../services/negocio/usuario/login/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

 constructor( private auth: AuthService,
 			  private router: Router ) {}

  canActivate(): boolean {

    if (this.auth.estaAutenticado()) {
    	return true;
    } else {
    	this.router.navigateByUrl('/sesion');
    	return false;
    }

  }
  canActivate2(): boolean {

    if (this.auth.estaAutenticado()) {
    	return false;
    } else {
    	this.router.navigateByUrl('/inicio');
    	return true;
    }

  }

}
