import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from '../services/negocio/usuario/login/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard2 implements CanActivate {

 constructor( private auth: AuthService,
 			  private router: Router ) {}


  canActivate(): boolean {

    if (this.auth.estaAutenticado()) {
        this.router.navigateByUrl('/registro-negocio');
    	return false;
    } else {
    	return true;
    }

  }

}
