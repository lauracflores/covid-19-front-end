import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { GlobalConstants } from '../../../shared/global-constants';

@Injectable({
providedIn: 'root'
})
export class YoutubeService {

apiKey : string = 'AIzaSyBh5NaIG8dHbMgRjtNExg4xajQDIAcbAnU';

constructor(public http: HttpClient) { }

  getVideosForChanel(channel, maxResults): Observable<Object> {
    let url =  GlobalConstants.apigeeBasepath +
    '/search?apikey=' + GlobalConstants.apigeeKey + '&key=' +
    this.apiKey + '&channelId=' + channel +
    '&order=date&part=snippet &type=video,id&maxResults=' + maxResults;
    return this.http.get(url)
      .pipe(map((res) => {
        return res;
      }));
  }
}
