import { Injectable } from '@angular/core';
import { HttpClient  } from '@angular/common/http';
import { GlobalConstants } from '../../../shared/global-constants';


@Injectable({
providedIn: 'root'
})
export class NewsApiService {

api_key = '1bfc68f5df2c473da035c55b601f3722';

constructor(private http:HttpClient) { }

  initArticles(){
    let url =  GlobalConstants.apigeeBasepath +
    '/top-headlines?apikey=' + GlobalConstants.apigeeKey +
    '&' + 'country=mx&category=health&apiKey=' + this.api_key;
    return this.http.get(url);
  }

}
