import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Pregunta } from 'src/app/models/diagnostico/preguntas/pregunta.model';
import { GlobalConstants } from '../../../shared/global-constants';

@Injectable({
  providedIn: 'root'
})
export class PreguntaService {
  //private diagnosisURL = `https://api.infermedica.com/covid19/diagnosis`;
  private diagnosisURL = GlobalConstants.apigeeBasepath +
  '/diagnosis?apikey=' + GlobalConstants.apigeeKey;

  constructor(private http: HttpClient) {}

  postDiagnosis = (sex: string, age: number, evidence: Object[]): Observable<Pregunta> => {
    const body = {
      sex,
      age,
      evidence,
    }
    const headers = {
      'Content-Type': 'application/json; charset=utf-8',
      'App-Id': 'a3074e30',
      'App-Key': 'fc264b3d75afb2ebfe8d6316b9ca179f',
      Accept: 'application/json'
    };

    return this.http.post<Pregunta>(`${this.diagnosisURL}`, body, { headers });
  };
}
