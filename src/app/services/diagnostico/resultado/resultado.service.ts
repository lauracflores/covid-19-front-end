import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Resultado } from 'src/app/models/diagnostico/resultado/resultado.model';
import { GlobalConstants } from '../../../shared/global-constants';

@Injectable({
    providedIn: 'root'
})

export class ResultadoService {
    //private diagnosisURL = `https://api.infermedica.com/covid19/triage`;
    private diagnosisURL = GlobalConstants.apigeeBasepath +
  '/triage?apikey=' + GlobalConstants.apigeeKey;

    constructor(private http: HttpClient) {}

    postTriage = (sex: string, age: number, evidence: Object[]): Observable<Resultado> => {
        const body = {
            sex,
            age,
            evidence,
          }
          const headers = {
            'Content-Type': 'application/json; charset=utf-8',
            'App-Id': 'a3074e30',
            'App-Key': 'fc264b3d75afb2ebfe8d6316b9ca179f',
            Accept: 'application/json'
          };     
        
        return this.http.post<Resultado>(`${this.diagnosisURL}`, body, { headers });
    }
}