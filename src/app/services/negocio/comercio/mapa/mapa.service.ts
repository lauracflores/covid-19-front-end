import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Negocio } from 'src/app/components/negocio/comercio/mapa/mapa.component';
import { Observable } from 'rxjs';
import { GlobalConstants } from '../../../../shared/global-constants';


@Injectable({providedIn: 'root'})

export class MapaService{

    url = GlobalConstants.apigeeBasepath;

    constructor(private http : HttpClient){}

    buscaNegocios(lat,lng,rad){
        rad = rad/1000;
        return this
        .http
        .get<Negocio>(`${this.url}/buscaNegocios?apikey=${GlobalConstants.apigeeKey}&lat=${lat}&lng=${lng}&radio=${rad}`).toPromise();
    }

    buscaComercios(lat,lng,rad):Observable<Negocio[]>{
        rad = rad/1000;
        return this
        .http
        .get<Negocio[]>(`${this.url}/buscaNegocios?apikey=${GlobalConstants.apigeeKey}&lat=${lat}&lng=${lng}&radio=${rad}`);
    }
    
    consultaNegocio(id){
        return this
        .http
        .get(`${this.url}/verificaRegistroNegocio?apikey=${GlobalConstants.apigeeKey}&idDenue=${id}`).toPromise();
    }

}