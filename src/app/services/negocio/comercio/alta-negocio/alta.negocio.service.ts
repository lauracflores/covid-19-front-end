import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Comercio } from 'src/app/models/negocio/comercio/comercio.model';
import { usuario } from 'src/app/models/negocio/usuario/usuario.model';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { GlobalConstants } from '../../../../shared/global-constants';


@Injectable({providedIn: 'root'})

export class AltaNegocioService{

    url = GlobalConstants.apigeeBasepath;

    constructor(private http : HttpClient, private router: Router){}

    verificaNegocio(nombre,entidad,municipio):Observable<Comercio[]>{
        let headers = new HttpHeaders().set('Authorization', localStorage.getItem('token'));
        let params = new HttpParams(); 
        params = params.append('nombre', nombre);
        params = params.append('entidad', entidad);
        params = params.append('municipio', municipio); 
        return this
        .http
        .get<Comercio[]>(`${this.url}/verificaNegocio?apikey=${GlobalConstants.apigeeKey}`,{headers : headers, params : params});
    }

    verificaRegistro(id):Observable<Comercio>{
        let headers = new HttpHeaders().set('Authorization', localStorage.getItem('token'));
        let params = new HttpParams(); 
        params = params.append('idDenue', id);
        return this
        .http
        .get<Comercio>(`${this.url}/verificaRegistroNegocio?apikey=${GlobalConstants.apigeeKey}`,{headers : headers, params : params});
    }

    registraNegocio(negocio){
        return this.verificaRegistro(negocio['Id']).subscribe((res: Comercio)=> {
            this.alertaEncontrado();
          },(err)=>{
            if(err.status == 404){
                this.postBody(negocio);
            }else{
                this.alertaErrorInesperado();
            }
        });
    }

    postBody(negocio){
        var array = negocio['Ubicacion'].split(',');
        var id = localStorage.getItem('id');
        const body = {
            idDenue: negocio['Id'],
            nombre: negocio['Nombre'],
            razonSocial: negocio['Razon_social'],
            calle: negocio['Calle'],
            numInt: negocio['Num_Interior'],
            numExt: negocio['Num_Exterior'],
            colonia: negocio['Colonia'],
            codigoPostal: negocio['CP'],
            estado: array[0],
            municipio: array[1],
            telefono: negocio['Telefono'],
            email: negocio['Correo_e'],
            sitioWeb: negocio['Sitio_internet'],
            longitud: negocio['Longitud'],
            latitud: negocio['Latitud'],
            descripcion: negocio['Clase_actividad'],
            idUsuario: Number(id)
        }
        let headers = new HttpHeaders().set('Authorization', localStorage.getItem('token'));
        this.http.post<usuario>(`${this.url}/negocios?apikey=${GlobalConstants.apigeeKey}`,body,{headers : headers}).subscribe(res=>{
            localStorage.setItem('idComercio', ""+res.id);
            this.alertaExito();
            this.router.navigate(["registro-horario"]);
        },err=>{
            this.alertaErrorInesperado();
        });
    }

    alertaErrorInesperado(){
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Ha ocurrido un error inesperado, porfavor intenta mas tarde.',
        });
    }

    alertaExito(){
        Swal.fire({
            icon: 'success',
            title: 'Tu negocio se ha dado de alta correctamente.',
            showConfirmButton: false,
            timer: 1500
        });
    }

    alertaEncontrado(){
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'El establecimiento ya está dado de alta en nuestro sistema, si deseas actualizar información porfavor comunicate con nostros.',
        });
    }

}