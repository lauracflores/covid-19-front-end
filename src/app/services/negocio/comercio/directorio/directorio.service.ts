import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Comercio} from '../../../../models/negocio/comercio/comercio.model';
import { GlobalConstants } from '../../../../shared/global-constants';

@Injectable({
  providedIn: 'root'
})
export class DirectorioService {

  constructor(private http: HttpClient) { }
  Url = GlobalConstants.apigeeBasepath +
  '/buscaNegocios?apikey=' + GlobalConstants.apigeeKey;

  getNegocioByRadio(lng:number,lat:number){
    return this.http.get<Comercio[]>(this.Url + '&lat=' + lat + '&lng=' + lng + '&radio=8');
  }
}
