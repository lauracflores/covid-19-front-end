import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DiasAtencion } from '../../../../models/negocio/comercio/dias-atencion.model';
import Swal from 'sweetalert2';
import { Comercio } from 'src/app/models/negocio/comercio/comercio.model';
import { GlobalConstants } from '../../../../shared/global-constants';

@Injectable({
  providedIn: 'root'
})
export class DiasAtencionService {

constructor(private http: HttpClient) { }

  Url = GlobalConstants.apigeeBasepath + '/';

  putNegocio(body){
    const httpOptions = {
      headers: new HttpHeaders({
                   'Content-Type': 'application/json',
                   'Authorization': localStorage.getItem('token')
      })};
    return this.http.put<Comercio>(this.Url + "negocios/" + localStorage.getItem('idComercio') + "?apikey="
                                    + GlobalConstants.apigeeKey,body,httpOptions);
  }

  getNegocioById(){
    return this.http.get<Comercio>(this.Url + "negocios/" + localStorage.getItem('idComercio') + "?apikey="
                                    + GlobalConstants.apigeeKey);
  }

  postDiaAtencion(diaAtencion: DiasAtencion){
    const httpOptions = {
        headers: new HttpHeaders({
                     'Content-Type': 'application/json',
                     'Authorization': localStorage.getItem('token')
        })};
      this.http.post<DiasAtencion>(this.Url + "diasAtencion?apikey=" + GlobalConstants.apigeeKey, diaAtencion, httpOptions).subscribe(res=>{
        Swal.fire('Dia registrado correctamente.')

    },err=>{
        Swal.fire('Ha ocurrido un error inesperado, porfavor intenta mas tarde.')
    });
  }
  putDiaAtencion(diaAtencion: DiasAtencion, id:number){
    const httpOptions = {
      headers: new HttpHeaders({
                   'Content-Type': 'application/json',
                   'Authorization': localStorage.getItem('token')
      })};
    return this.http.put<DiasAtencion>(this.Url + "diasAtencion/" +id + "?apikey=" 
                                      + GlobalConstants.apigeeKey, diaAtencion, httpOptions).subscribe(res=>{
      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'Dia actualizado correctamente',
        showConfirmButton: false,
        timer: 1500
      })
      window.location.reload();

  },err=>{
    console.log(err);
      Swal.fire('Ha ocurrido un error inesperado, porfavor intenta mas tarde.')
  });
  }
  getHorario(idNegocio: number){
    return this.http.get<DiasAtencion[]>(this.Url + "negocios/" + idNegocio+"/horario"+ "?apikey=" 
                                        + GlobalConstants.apigeeKey).subscribe(res=> {
    },(err)=>{
     
      Swal.fire('Ha ocurrido un error inesperado, porfavor intenta mas tarde.')
      
  });
  }
  getHorario2(idNegocio: number){
    return this.http.get<DiasAtencion[]>(this.Url + "negocios/" + idNegocio+"/horario"+ "?apikey=" 
                                        + GlobalConstants.apigeeKey);
  }

  deleteDiaAtencion (id: number) {
    const httpOptions = {
      headers: new HttpHeaders({
                   'Content-Type': 'application/json',
                   'Authorization': localStorage.getItem('token')
      })};
      return this.http.delete(this.Url + "diasAtencion/" +id+ "?apikey=" 
      + GlobalConstants.apigeeKey, httpOptions).subscribe(res=> {
      },(err)=>{
       
        Swal.fire('Ha ocurrido un error inesperado, porfavor intenta mas tarde.')
        
    });
    }

}
