import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Comercio } from '../../../../models/negocio/comercio/comercio.model';
import { GlobalConstants } from '../../../../shared/global-constants';

@Injectable({
    providedIn: 'root'
  })
  export class ComercioService {
  
  constructor(private http: HttpClient) { }
  
    Url = GlobalConstants.apigeeBasepath + '/';

    getNegociobyIdUsuario(id: number){
        const httpOptions = {
          headers: new HttpHeaders({
                       'Content-Type': 'application/json',
                       'Authorization': localStorage.getItem('token')
          })};
        return this.http.get<Comercio[]>(this.Url + "negocios?apikey=" 
                                        + GlobalConstants.apigeeKey + "&usuario=" + id, httpOptions);
      }

      deleteNegociobyId(comercio: Comercio){
        const httpOptions = {
          headers: new HttpHeaders({
                       'Content-Type': 'application/json',
                       'Authorization': localStorage.getItem('token')
          })};
          return this.http.delete<Comercio>(this.Url + "negocios/" + comercio.id + "?apikey=" + GlobalConstants.apigeeKey, httpOptions);
      }

}