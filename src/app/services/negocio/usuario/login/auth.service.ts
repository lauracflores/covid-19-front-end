import {Injectable} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { usuario } from 'src/app/models/negocio/usuario/usuario.model';
import {map} from 'rxjs/operators';
import { GlobalConstants } from '../../../../shared/global-constants';

@Injectable({
    providedIn: 'root'
})
export class AuthService{
    private url = GlobalConstants.apigeeBasepath;
    userToken: string;
    constructor(private http: HttpClient){
        this.leerToken();
    }

    logout(){
        localStorage.removeItem('token');
        localStorage.removeItem('id');
        localStorage.removeItem('userbyuser');
    }
    login(usuario: usuario){
        localStorage.setItem('userbyuser', usuario.nombreUsuario.toString());
        const httpOptions = {
            headers: new HttpHeaders({
                         'Content-Type': 'application/json',
                         'Access-Control-Allow-Origin': '*',
                         'Access-Control-Allow-Credentials': 'true'
            })};
        const authData={
            usuario: usuario.nombreUsuario,
            password: usuario.password
        };
        return this.http.post(
            `${this.url}/login?apikey=` + GlobalConstants.apigeeKey + `&usuario=${usuario.nombreUsuario}&password=${usuario.password}`,
            authData,httpOptions
        ).pipe(
            map(resp=>{
                this.guardarToken(resp['token']);
                return resp;
            })
        )
    }
    private guardarToken(idToken: string){
        this.userToken=idToken;
        localStorage.setItem('token', idToken);
        let hoy = new Date();
        hoy.setSeconds(1800);
        localStorage.setItem('expira', hoy.getTime().toString());


    }
    leerToken(){
        if(localStorage.getItem('token')){
            this.userToken=localStorage.getItem('token');

        }else{
            this.userToken='';
        } 
        return this.userToken;
    }

    estaAutenticado (): boolean {
        if(this.userToken.length<2){
            return false;
        }
         const expira=Number(localStorage.getItem('expira'));
         const expiraDate= new Date();
         expiraDate.setTime(expira)
         if(expiraDate>new Date()){
             return true;
         }else{
             return false;
         }

    }

}
