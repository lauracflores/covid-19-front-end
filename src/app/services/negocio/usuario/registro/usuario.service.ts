import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { usuario } from '../../../../models/negocio/usuario/usuario.model';
import { AuthService } from '../login/auth.service';
import { GlobalConstants } from '../../../../shared/global-constants';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

constructor(private http: HttpClient, private auth: AuthService) { }

  Url = GlobalConstants.apigeeBasepath


  getusuarioById(id: number){
    const httpOptions = {
      headers: new HttpHeaders({
                   'Content-Type': 'application/json',
                   'Authorization': localStorage.getItem('token')
      })};
    return this.http.get<usuario>(this.Url + "/usuarios/" + id + "?apikey=" + GlobalConstants.apigeeKey, httpOptions);
  }

  getUsuarioByEmail(email: String){
    return this.http.get<usuario>(this.Url + "/usuarios?apikey=" + GlobalConstants.apigeeKey +"&email="+email);
  }

  getUsuarioByUsuario(usuario: String){
    return this.http.get<usuario>(this.Url + "/usuarios?apikey=" + GlobalConstants.apigeeKey +"&usuario="+usuario);
  }

  postusuario(usuario: usuario){
    return this.http.post<usuario>(this.Url + "/usuarios?apikey=" + GlobalConstants.apigeeKey, usuario);
  }

  getusuario(usuariobyusuario: string){
    const httpOptions = {
      headers: new HttpHeaders({
                   'Content-Type': 'application/json',
                   'Authorization': localStorage.getItem('token')
      })};
    return this.http.get<usuario>(this.Url + "/usuarios?apikey=" + GlobalConstants.apigeeKey +"&usuario=" + usuariobyusuario, httpOptions);
  }

  putusuario(usuario: usuario){
    const httpOptions = {
      headers: new HttpHeaders({
                   'Content-Type': 'application/json',
                   'Authorization': localStorage.getItem('token')
      })};
    return this.http.put<usuario>(this.Url + "/usuarios/" + usuario.id + "?apikey=" + GlobalConstants.apigeeKey, usuario, httpOptions);
  }

}
