import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { GlobalConstants } from '../../../shared/global-constants';

@Injectable({
providedIn: 'root'
})
export class GraficasGobiernoService {

constructor(public http: HttpClient) { }

  getCasosByState(state: string): Observable<Object> {
    let url = GlobalConstants.apigeeBasepath +
     '/casos/' + state + '?apikey=' + GlobalConstants.apigeeKey;
    return this.http.get(url);
  }

  getMuertesByEnfermedad(state: string): Observable<Object> {
    let url = GlobalConstants.apigeeBasepath +
    '/casos/' + state + '/disease?apikey=' + GlobalConstants.apigeeKey;
    return this.http.get(url);
  }

  getTimeline(state: string, type: string): Observable<Object> {
    let url = GlobalConstants.apigeeBasepath +
    '/casos/' + state + '/timeline?apikey=' + GlobalConstants.apigeeKey + '&type=' + type;
    return this.http.get(url);
  }
}
