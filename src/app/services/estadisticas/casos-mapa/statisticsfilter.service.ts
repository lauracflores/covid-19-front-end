import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GlobalConstants } from '../../../shared/global-constants';
import { Observable, of } from 'rxjs';

@Injectable({
providedIn: 'root'
})
export class StatisticsFilterService {

constructor(public http: HttpClient) { }

  getMexicoStatistics(): Observable<Object> {
    let url =  GlobalConstants.apigeeBasepath +
              '/datas?apikey=' + GlobalConstants.apigeeKey +
              '&country=Mexico';
    return this.http.get(url);
  }
}