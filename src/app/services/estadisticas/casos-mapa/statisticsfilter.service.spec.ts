import { TestBed } from '@angular/core/testing';

import { StatisticsFilterService } from './statisticsfilter.service';

describe('StatisticsFilterService', () => {
  let service: StatisticsFilterService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StatisticsFilterService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});