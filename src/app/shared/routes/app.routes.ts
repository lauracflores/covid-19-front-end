import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InicioComponent } from '../layout/inicio/inicio.component';
import {InicioSesionComponent} from '../../components/negocio/usuario/inicio-sesion/inicio-sesion.component';
import {RegistroNegocioComponent} from '../../components/negocio/comercio/registro/registro-negocio.component';
import {RegistroNegocioHorarioComponent} from '../../components/negocio/comercio/registro/registro-negocio-horario/registro-negocio-horario.component';
import {RegistroUsuarioComponent} from '../../components/negocio/usuario/registro/registro-usuario.component';
import {DiagnosticoComponent} from '../../components/diagnostico/diagnostico.component';
import {GraficasGobiernoComponent} from '../../components/estadisticas/graficas-gobierno/graficas-gobierno.component';
import { MapaComponent } from '../../components/negocio/comercio/mapa/mapa.component';
import { AuthGuard } from '../../guards/auth.guard';
import { AuthGuard2 } from 'src/app/guards/auth.guard2';
import { PerfilComponent } from '../../components/negocio/usuario/perfil/perfil.component';
import {ActualizarNegocioHorarioComponent} from '../../components/negocio/comercio/actualizar/actualizar-negocio-horario/actualizar-negocio-horario.component';



const APP_ROUTES: Routes = [
  {path: 'inicio', component: InicioComponent},
  {path: 'sesion', component: InicioSesionComponent, canActivate: [ AuthGuard2 ]},
  {path: 'registro-usuario', component: RegistroUsuarioComponent, canActivate: [ AuthGuard2 ]},
  {path: 'diagnostico', component: DiagnosticoComponent},
  {path: 'graficas-gobierno', component: GraficasGobiernoComponent},
  {path: 'mapa', component: MapaComponent},
  {path: 'registro-horario', component: RegistroNegocioHorarioComponent},
  {path: 'registro-negocio', component: RegistroNegocioComponent, canActivate: [ AuthGuard ]},
  {path: 'perfil', component: PerfilComponent, canActivate: [ AuthGuard ]},
  {path: 'actualizar-horario', component: ActualizarNegocioHorarioComponent},
  {path: '**', pathMatch: 'full', redirectTo: 'inicio'}
];

@NgModule({
	imports: [ RouterModule.forRoot(APP_ROUTES) ],
	exports: [ RouterModule ]
})

export class APP_ROUTING {}
//export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);
