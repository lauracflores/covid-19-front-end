import { Component, OnInit } from '@angular/core'; 
import {Router} from '@angular/router'
import {AuthService } from '../../../services/negocio/usuario/login/auth.service'

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(private auth:AuthService, private router: Router) { }

  ngOnInit(): void {
  }
  salir(){
  this.auth.logout();
  this.auth.leerToken();
  //this.router.navigateByUrl('/sesion');
  
  }
  estaAutenticado(){
    if(this.auth.estaAutenticado()){
      return true;
    }
    return false;
  }
}
