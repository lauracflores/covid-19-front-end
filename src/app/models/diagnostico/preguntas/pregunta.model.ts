class Choice {
    id: string;
    label: string;
  }
  
  class Item {
    choices: Choice[];
    explanation: string;
    id: string;
    name: string;
  }
  
  class Question {
    explanation: any;
    extras: any;
    items: Item[];
    text: string;
    type: string;
  }
  
  export class Pregunta {
    conditions: [];
    extras: any;
    question: Question;
    should_stop: boolean;
  }
