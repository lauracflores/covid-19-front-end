export class usuario{

    id: number;
    nombre: String;
    apellidoPaterno: String;
    apellidoMaterno: String;
    nombreUsuario: String;
    password: String;
    correo: String;
    telefono: String;

}
