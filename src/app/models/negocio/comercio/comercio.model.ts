import { DiasAtencion } from './dias-atencion.model';

export class Comercio{
    id:number;
    nombre: String;
    razonSocial: String;
    calle: String;
    numInt: number;
    numExt: number;
    colonia: String;
    codigoPostal: number;
    estado: String;
    municipio: String;
    telefono: number;
    email: String;
    sitioWeb: String;
    longitud: number;
    latitud: number;
    descripcion: String;
    diasAtencion: DiasAtencion[];

}