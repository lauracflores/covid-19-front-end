export class DiasAtencion{
    id:number;
    dia: string;
    abierto: number;
    horaApertura: string;
    horaCierre: string;    
    negocio: number;
}