import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NewsApiService } from './services/informativos/blogs/news-api.service';
import { APP_ROUTING } from './shared/routes/app.routes';
import {MatFormFieldModule} from '@angular/material/form-field';
import { MatTableModule } from '@angular/material/table';
import { MatInputModule } from '@angular/material/input';
import {AgmMap, MouseEvent,MapsAPILoader, AgmCoreModule  } from '@agm/core';
import { AppComponent } from './shared/app.component';
import { BlogsInformativosComponent } from './components/informativos/blogs/blogs-informativos.component';
import { CasosMapaComponent } from './components/estadisticas/casos-mapa/casos-mapa.component';
import { NavbarComponent } from './shared/layout/navbar/navbar.component';
import { InicioSesionComponent } from './components/negocio/usuario/inicio-sesion/inicio-sesion.component';
import { RegistroUsuarioComponent } from './components/negocio/usuario/registro/registro-usuario.component';
import { RegistroNegocioComponent } from './components/negocio/comercio/registro/registro-negocio.component';
import { InicioComponent } from './shared/layout/inicio/inicio.component';
import { DiagnosticoComponent } from './components/diagnostico/diagnostico.component';
import { DatosDiagnosticoComponent } from './components/diagnostico/datos-usuario/datos-usuario.component';
import { PreguntasComponent } from './components/diagnostico/preguntas/preguntas.component';
import { ResultadoComponent } from './components/diagnostico/resultado/resultado.component';
import { VideosComponent } from './components/informativos/videos/videos.component';
import { FooterComponent } from './shared/layout/footer/footer.component';
import { WhatsappComponent } from './components/informativos/whatsapp/whatsapp.component';
import {DirectorioComponent} from './components/negocio/comercio/directorio/directorio.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { GraficasGobiernoComponent } from'./components/estadisticas/graficas-gobierno/graficas-gobierno.component';
import {ChartsModule} from "ng2-charts";
import { MapaComponent } from './components/negocio/comercio/mapa/mapa.component';
import { MapaService } from './services/negocio/comercio/mapa/mapa.service';
import { MatPaginatorModule } from '@angular/material/paginator';
import { UsernameValidator } from './components/negocio/usuario/registro/username-validator.directive';
import { PerfilComponent } from './components/negocio/usuario/perfil/perfil.component';
import {MatExpansionModule} from '@angular/material/expansion';
import { RegistroNegocioHorarioComponent } from './components/negocio/comercio/registro/registro-negocio-horario/registro-negocio-horario.component';
import { ActualizarNegocioHorarioComponent } from './components/negocio/comercio/actualizar/actualizar-negocio-horario/actualizar-negocio-horario.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {ConfirmEqualValidatorDirective} from '../app/components/negocio/usuario/registro/confirm-equal-validator.directive'

@NgModule({
  declarations: [
    AppComponent,
    BlogsInformativosComponent,
    CasosMapaComponent,
    NavbarComponent,
    InicioSesionComponent,
    RegistroUsuarioComponent,
    RegistroNegocioComponent,
    InicioComponent,
    DiagnosticoComponent,
    PreguntasComponent,
    ResultadoComponent,
    DatosDiagnosticoComponent,
    VideosComponent,
    FooterComponent,
    WhatsappComponent,
    DirectorioComponent,
    GraficasGobiernoComponent,
    MapaComponent,
    PerfilComponent,
    RegistroNegocioHorarioComponent,
    ActualizarNegocioHorarioComponent,
    ConfirmEqualValidatorDirective
  ],
  imports: [
    BrowserModule,
    FormsModule,
    APP_ROUTING,
    FormsModule,
    HttpClientModule,
    MatTableModule,
    MatFormFieldModule,
    MatInputModule,
    MatPaginatorModule,
    AgmCoreModule.forRoot({  
      apiKey: 'AIzaSyABtrdU6ueytGAYg-uONZpJuE24k8cblVI'  
    }),  
    NoopAnimationsModule,
    ChartsModule,
    MatPaginatorModule,
    ReactiveFormsModule,
    MatExpansionModule,
    BrowserAnimationsModule
  ],
  exports:[
    MatTableModule,
    MatFormFieldModule,
    MatInputModule,
    MatPaginatorModule
  ],
providers: [NewsApiService,MapaService,UsernameValidator],
bootstrap: [AppComponent],
schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
