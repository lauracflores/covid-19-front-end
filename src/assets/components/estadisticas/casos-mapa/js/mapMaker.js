var datosEstados = new Object();
var json;
var estatesForGraphic;

var totalCasesMexico;
var totalDeathsMexico;
var totalRecoveredMexico;
var casesIncrementMexico;
var deathsIncrementMexico;
var recoveredIncrementMexico;
var casesIncrementMexicoPercentage;
var deathsIncrementMexicoPercentage;
var recoveredIncrementMexicoPercentage;
var mortalityRateMexico;
var recoveredRateMexico;
var lastUpdate;

var country;
var states;

function paintMap(myJson) {
    getStateDatas(myJson);
    createMap();
    fillMexicoDatas();
}

function paintGraphic(myJson) {
    getStateDatas(myJson);
    fillMexicoDatas();
    populateTable(estatesForGraphic);
}

function getStateDatas(countries) {

    for (let value of countries) {
        country = value;
    }

    totalCasesMexico = country.totalConfirmed;
    totalDeathsMexico = country.totalDeaths;
    totalRecoveredMexico = country.totalRecovered;
    casesIncrementMexico = country.totalConfirmedDelta;
    deathsIncrementMexico = country.totalDeathsDelta;
    recoveredIncrementMexico = country.totalRecoveredDelta;
    casesIncrementMexicoPercentage = country.confirmedIncrementPercentage;
    deathsIncrementMexicoPercentage = country.mortalityIncrementPercentage;
    recoveredIncrementMexicoPercentage = country.recoveredIncrementPercentage;
    mortalityRateMexico = country.mortalityRate;
    recoveredRateMexico = country.recoveredRate;
    lastUpdate = country.lastUpdated;

    states = country.areas;
    estatesForGraphic = states;

    for (let value of states) {

        var stateName = value.displayName;

        var stateId = getStateId(stateName);

        var cases = value.totalConfirmed;
        var casesDelta = value.totalConfirmedDelta;
        var caseIncrementPer = value.confirmedIncrementPercentage;


        var totalDeaths = value.totalDeaths;
        var mortalityRate = value.mortalityRate;
        var deathsDelta = value.totalDeathsDelta;
        var deathsIncrementPer = value.mortalityIncrementPercentage;

        var totalRecovered = value.totalRecovered;
        var recoveredRate = value.recoveredRate;
        var recoveredDelta = value.totalRecoveredDelta;
        var recoveredIncrementPer = value.recoveredIncrementPercentage;

        var lastStateUpdate = value.lastUpdated;

        var infectionPercentage = ((cases * 100) / totalCasesMexico).toFixed(2);

        var childObject = new Object();

        childObject['name'] = stateName;

        childObject['infectionPercentage'] = infectionPercentage;

        childObject['cases'] = cases;
        childObject['caseIncrementDelta'] = casesDelta;
        childObject['caseIncrementPer'] = caseIncrementPer;

        childObject['totalDeaths'] = totalDeaths;
        childObject['mortalityRate'] = mortalityRate;
        childObject['deathsIncrementDelta'] = deathsDelta;
        childObject['deathsIncrementPer'] = deathsIncrementPer;

        childObject['totalRecovered'] = totalRecovered;
        childObject['recoveredRate'] = recoveredRate;
        childObject['recoveredIncrementDelta'] = recoveredDelta;
        childObject['recoveredIncrementPer'] = recoveredIncrementPer;

        childObject['lastUpdate'] = lastStateUpdate;

        var fill;

        fill = 'NODATA';

        if (mortalityRate == 0)
            fill = 'ZERO';
        if (mortalityRate > 0 && mortalityRate < 5)
            fill = 'LOW';
        if (mortalityRate >= 5 && mortalityRate < 10)
            fill = 'MEDIUM';
        if (mortalityRate >= 10 && mortalityRate < 15)
            fill = 'HIGH';
        if (mortalityRate >= 15)
            fill = 'WARNING';


        childObject['fillKey'] = fill;

        datosEstados[stateId] = childObject;
    }
    json = datosEstados;
}


function createMap() {
    var map = new Datamap({
        element: document.getElementById('container'),
        scope: 'mex',
        data: json,
        fills: {
            ZERO: 'green',
            LOW: 'rgb(217, 255, 0)',
            MEDIUM: 'rgb(255, 234, 0)',
            HIGH: 'rgb(255, 149, 0)',
            WARNING: 'rgb(255, 17, 0)',
            UNKNOWN: 'rgb(0,0,0)',
            defaultFill: 'gray'
        }
    });
}

function fillMexicoDatas() {
    var mexDatas = document.getElementById("mexico_datas");
    mexDatas.innerHTML = `<h4>Covid en México</h4><br/>
<b style="font-size: 15px;">Casos Confirmados: </b> 
<b style="font-size: 15px; color: red;">${totalCasesMexico}</b><br/>
<b style="font-size: 15px;">Incremento de casos desde ultima actualización: </b> 
<b style="font-size: 15px; color: red;">${casesIncrementMexico}</b><br/>
<b style="font-size: 15px;">Porcentaje de incremento de casos desde ultima actualización: </b> 
<b style="font-size: 15px; color: red;">${casesIncrementMexicoPercentage.toFixed(2)}%</b><br/>
<b style="font-size: 15px;">Muertes Confirmadas: </b> 
<b style="font-size: 15px; color: red;"> ${totalDeathsMexico}</b><br/>
<b style="font-size: 15px;">Tasa de Mortalidad: </b> 
<b style="font-size: 15px; color: red;"> ${mortalityRateMexico.toFixed(2)}</b><br/>
<b style="font-size: 15px;">Incremento de muertes desde ultima actualización: </b>
<b style="font-size: 15px; color: red;">${deathsIncrementMexico}</b><br/>
<b style="font-size: 15px;">Porcentaje de incremento de muertes desde ultima actualización: </b>
<b style="font-size: 15px; color: red;">${deathsIncrementMexicoPercentage.toFixed(2)}%</b><br/>
<b style="font-size: 15px;">Total de recuperados: </b> 
<b style="font-size: 15px; color: green;"> ${totalRecoveredMexico}</b><br/>
<b style="font-size: 15px;">Tasa de recuperación: </b> 
<b style="font-size: 15px; color: green;"> ${recoveredRateMexico}</b><br/>
<b style="font-size: 15px;">Incremento de recuperados desde ultima actualización: </b>  
<b style="font-size: 15px; color: green;">${recoveredIncrementMexico}</b><br/>
<b style="font-size: 15px;">Porcentaje de incremento de recuperados desde ultima actualización: </b>  
<b style="font-size: 15px; color: green;">${recoveredIncrementMexicoPercentage.toFixed(2)}%</b><br/><br/>`
}

function getStateId(stateName) {
    switch (stateName) {
        case 'Baja California':
            return 'MX.BN';
        case 'Baja California Sur':
            return 'MX.BS';
        case 'Coahuila de Zaragoza':
            return 'MX.CA';
        case 'Coahuila':
            return 'MX.CA';
        case 'Chihuahua':
            return 'MX.CH';
        case 'Durango':
            return 'MX.DU';
        case 'Sinaloa':
            return 'MX.SI';
        case 'Sonora':
            return 'MX.SO';
        case 'Zacatecas':
            return 'MX.ZA';
        case 'Nuevo León':
            return 'MX.NL';
        case 'San Luis Potosí':
            return 'MX.SL';
        case 'Tamaulipas':
            return 'MX.TM';
        case 'Aguascalientes':
            return 'MX.AG';
        case 'Colima':
            return 'MX.CL';
        case 'Jalisco':
            return 'MX.JA';
        case 'Michoacán de Ocampo':
            return 'MX.MC';
        case 'Michoacán':
            return 'MX.MC';
        case 'Nayarit':
            return 'MX.NA';
        case 'Campeche':
            return 'MX.CM';
        case 'Oaxaca':
            return 'MX.OA';
        case 'Puebla':
            return 'MX.PU';
        case 'Tabasco':
            return 'MX.TB';
        case 'Morelos':
            return 'MX.MR';
        case 'Querétaro Arteaga':
            return 'MX.QE';
        case 'Querétaro':
            return 'MX.QE';
        case 'Veracruz de Ignacio de la Llave':
            return 'MX.VE';
        case 'Veracruz':
            return 'MX.VE';
        case 'Tlaxcala':
            return 'MX.TL';
        case 'México':
            return 'MX.DF';
        case 'Guanajuato':
            return 'MX.GJ';
        case 'Guerrero':
            return 'MX.GR';
        case 'Hidalgo':
            return 'MX.HI';
        case 'Ciudad de México':
            return 'MX.MX';
        case 'Mexico City':
            return 'MX.MX';
        case 'Chiapas':
            return 'MX.CP';
        case 'Quintana Roo':
            return 'MX.QR';
        case 'Yucatán':
            return 'MX.YU';
        default:
            return 'No Code';
    }
}

function populateTable(states) {
    var space = document.getElementById("chartSpace");
    var textTable;
    textTable = `<table class="highchart" data-graph-container-before="1" data-graph-type="column">
                        <caption>Casos confirmados y muertes totales por estado en Mexico</caption>
                        <thead>
                        <tr>
                            <th>Codigo</th>
                            <th data-graph-skip="1">Estado</th>
                            <th data-graph-color="blue">Casos Confirmados</th>
                            <th data-graph-color="red">Muertes</th>
                        </tr>
                        </thead>
                        <tbody>`;

    for (let state of states) {
        textTable += `<tr>`;
        textTable += `<td>`;
        textTable += getStateId(state.displayName).split(".")[1];
        textTable += `</td>`;
        textTable += `<td data-graph-hidden="1">`;
        textTable += state.displayName;
        textTable += `</td>`;
        textTable += `<td>`;
        textTable += state.totalConfirmed;
        textTable += `</td>`;
        textTable += `<td>`;
        textTable += state.totalDeaths;
        textTable += `</td>`;
        textTable += `</tr>`;
    }

    textTable += `</tbody></table>`;

    space.innerHTML = textTable;


    $(document).ready(function() {
        $('table.highchart').highchartTable();
    });

}